public class CaracterePositionne{
  double x,y;
  char c;
  public CaracterePositionne(double a,double b, char d){
    x=a;
    y=b;
    c=d;
  }

  public double getX(){
    return x;
  }

  public double getY(){
    return y;
  }

  //permet de set des position au cas où le caractere doit changer de position.
  public void setPos(double x,double y){
    this.x=x;
    this.y=y;
  }

}
