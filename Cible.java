public class Cible{
  double x,y;
  EnsembleCaracteres z;

  public Cible(double x, double y){
    this.x=x;
    this.y=y;
    z=new EnsembleCaracteres();
  }

  public void ajouter(double x, double y, char c){
    z.ajouteCar(x,y,c);
  }

  public EnsembleCaracteres getEnsembleCaracteres(){
    return z;
  }

  public double getX(){
    return this.x;
  }

  public double getY(){
    return this.y;
  }

  public double getMaxX(){
    double max = z.getFirst().getX();

    for(CaracterePositionne m:z.getCarac()){
      if(m.getX()>max){
        max=m.getX();
      }
    }
    return max;
  }

  public double getMaxY(){
    double max = z.getFirst().getY();
    for(CaracterePositionne m:z.getCarac()){
      if(m.getY()>max){
        max=m.getY();
      }
    }
    return max;
  }

  public void creerCibleDefaut(){
    ajouter(getX(),getY(),'#');
    ajouter(getX()+1,getY(),'#');
    ajouter(getX(),getY()+1,'#');
    ajouter(getX()+1,getY()+1,'#');
  }

}
