import java.util.ArrayList;
public class EnsembleCaracteres{
  ArrayList<CaracterePositionne> caracteres;

  public EnsembleCaracteres(){
    caracteres = new ArrayList<CaracterePositionne>();
  }

  public void ajouteCar(double x, double y, char c){
    caracteres.add(new CaracterePositionne(x,y,c));
  }

  public void vider(){
    caracteres.clear();
  }

  public ArrayList<CaracterePositionne> getCarac(){
    return caracteres;
  }

  void union(EnsembleCaracteres e){
    for(CaracterePositionne c : e.getCarac()){
      this.caracteres.add(c);
    }
  }

  public CaracterePositionne getFirst(){
    return caracteres.get(0);
  }
}
