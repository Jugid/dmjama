import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;

public class GorillaBomb{
    Projectile p;
    Cible c;
    Obstacle o;
    EnsembleCaracteres ens;
    ArrayList<Joueur> joueurs;
    Random r = new Random();
    String [] obst = {
    "    /|  ____",
    "    <> ( oo )",
    "    <>_| ^^ |_",
    "    <>   @    |",
    "   /~~| . . _ |",
    "  /~~~~|    | |",
    " /~~~~~~|/ _| |",
    " |[][][]/ / [m]",
    " |[][][[m]",
    " |[][][]|",
    " |[][][]|",
    " |[][][]|",
    " |[|--|]|",
    " |[|  |]|",
    " ========",
    "==========",
    "|[[    ]]|",
    "==========",};

    GorillaBomb(){
      //Création d'un nouveau projectile
      p= new Projectile(1,1);
      p.creerProjectileDefaut();

      //Creation d'une cible a un Y aléatoire
      c = new Cible(58,20+r.nextInt(15));
      c.creerCibleDefaut();

      //Création d'un obstacle à un X aléatoire
      o=new Obstacle(30+(r.nextInt(20)),0,obst);
      joueurs=new ArrayList<Joueur>();

      this.ens=new EnsembleCaracteres();
      this.ens.union(c.getEnsembleCaracteres());
      this.ens.union(p.getEnsembleCaracteres());
      this.ens.union(o.getEnsembleCaracteres());
  }

  //Permet d'ajouter des joueurs à la liste selon le nombre de joueurs donné
  void  creerJoueurs(int nbJoueurs){
    int i=1;
    while(i<nbJoueurs+1){
      joueurs.add(new Joueur(i));
      i++;
    }
  }

  //Met fin au jeu si un joueur à au moins 5 points
  boolean fin(){
    for(int i = 0; i < joueurs.size();i++){
      if(joueurs.get(i).getScore()>=5){
        System.out.println("C'est le joueur " + i+1 + " qui gagne !!");
        return true;
      }
    }
    return false;
  }

  //demande à l'utilisateur une force, un angle afin de faire bouger le projectile
  void debutTour(Fenetre fenetre){
    double f,a;
    Scanner sc = new Scanner(System.in);
    do{
      System.out.print("Force : ");
      String force= sc.nextLine();
      f = Double.parseDouble(force);
    }while(f>100 || f<=0);

    do{
      System.out.print("Angle : ");
      String angle = sc.nextLine();
      a = Double.parseDouble(angle);
    }while(a>90 || a<=0);

    //On créé le projectile qui va etre utilisé pour le tour
    p= new Projectile(1,1);
    p.creerProjectileDefaut();
    p.setVitesses(f*Math.cos((a/180)*Math.PI),f*Math.sin((a/180)*Math.PI));
  }

  //met fin à la boucle du tour de joueur. Si le joueur touche la cible, il gagne un point.
  boolean finTour(Fenetre fenetre,Joueur j){
    if(p.estDans(fenetre)){
      return true;
    }
    else{
      if(p.toucheO(o)){
        return true;
      }
      else{
        if(p.touche(c)){
          j.addScore(1);
          return true;
        }
      }
    }
    return false;
  }

  //Affiche les caracteres
  void affiche(Fenetre fenetre){
    fenetre.AfficherCaracteres(this.ens);
  }

  //Fait bouger le projectile selon les formule mathématiques données dans le sujet et prend aussi le vent en compte
  //Vide l'ensemble et le reforme avec les nouvelles données
  void evolue(Fenetre fenetre,Vent vent){
    p.bouge(vent);
    this.ens.vider();
    this.ens.union(c.getEnsembleCaracteres());
    this.ens.union(p.getEnsembleCaracteres());
    this.ens.union(o.getEnsembleCaracteres());
  }

  ArrayList<Joueur> getEnsembleJoueurs(){
    return joueurs;
  }

  //Affiche les règles au tout début. Pour eviter de mettre ces lignes dans le main
  void regles(){
    System.out.println("");
    System.out.println("Bienvenu sur GorillaBomb");
    System.out.println("Le but est de toucher la cible en contournant l'obstacle");
    System.out.println("Mais attention ! Il y a du vent, d'une force et d'une direction aléatoire.");
    System.out.println("Ce dernier peut grandement changer la trajectoire de votre bombe alors, attention !");
    System.out.println("");
  }

  //Créé la nouvelle cible, le nouvel obstacle
  void change(Fenetre fenetre){
    c=new Cible(58,20+r.nextInt(15));
    c.creerCibleDefaut();
    o=new Obstacle(30+r.nextInt(10),0,obst);
    this.ens.vider();
    this.ens.union(c.getEnsembleCaracteres());
    this.ens.union(p.getEnsembleCaracteres());
    this.ens.union(o.getEnsembleCaracteres());
    affiche(fenetre);
  }
}
