import java.awt.Color;

public class Joueur{
  int numero,score;
  Color couleurJ;

  Joueur(int n){
    this.numero=n;
    this.couleurJ=new Color((int) (Math.random() * (double) (0xFFFFFF) / 2));
  }

  public int getScore(){
    return score;
  }

  public int getNumJoueur(){
    return this.numero;
  }

  public Color getColor(){
    return this.couleurJ;
  }

  public void setScore(int nombre){
    this.score = nombre;
  }

  public void addScore(int nombre){
    this.score+=nombre;
  }
}
