import java.util.Scanner;
import java.util.Random;
public class Main{
  public static void main(String [] args){

    //Initialisation des objets et variables
    Fenetre fenetre = new Fenetre();
    GorillaBomb jeu = new GorillaBomb();
    int nbJoueurs;
    Random r=new Random();
    jeu.regles();

    //Indications pour l'utilisateur
    System.out.println("Inscription des joueurs");

    //Scanner pour l'entrée de l'utilisateur
    Scanner sc=new Scanner(System.in);
    do{
      System.out.print("Combien de joueurs (max 10) ? ");
      String nb = sc.nextLine();
      nbJoueurs=Integer.parseInt(nb);
    }while(nbJoueurs>10 || nbJoueurs<=0);

    //On ajouet les joueurus au jeu

    jeu.creerJoueurs(nbJoueurs);
    //Le jeu commence
    int joueurCourant=1;
    while(!jeu.fin()){//Le joueur qui obtient 5 points gagne !
      fenetre.setColor(jeu.getEnsembleJoueurs().get(joueurCourant-1).getColor()); //Chaque joueur est différencié par son numéro et sa couleur (générée aléatoirement)
      System.out.println("C'est au joueur "+joueurCourant+" de jouer. SCORE : "+jeu.getEnsembleJoueurs().get(joueurCourant-1).getScore());//On affiche quel joueur joue et quel score il a
      //Creation du vent, aleatoire a chaque tour
      Vent vent = new Vent();
      //affichage du vent pour le joueur en cours.
      System.out.println("Le vent à une force de "+Math.round(vent.getForce()*30)+" et "+ vent.getPhrase());

      jeu.affiche(fenetre);
      jeu.debutTour(fenetre);
      while(!jeu.finTour(fenetre,jeu.getEnsembleJoueurs().get(joueurCourant-1))){
        jeu.evolue(fenetre,vent);
        fenetre.pause(60);
        jeu.affiche(fenetre);
      }
      jeu.change(fenetre);

      //switch du joueur pour affichage
      if(joueurCourant==nbJoueurs){joueurCourant=1;}
      else{joueurCourant+=1;}
    }
    System.out.println("Fin du jeu");
}
}
