public class Obstacle{
  double x,y;
  EnsembleCaracteres sprite;

  Obstacle(double x, double y,String [] tableau){
    this.x=x;
    this.y=y;
    this.sprite=creeTableau(tableau);
  }

  //Créé l'EnsembleCaracteres nécessaire pour afficher l'obstacle.
  //parcours le tableau, puis parcours caracteres par caracteres pour leur donner des positions

  private EnsembleCaracteres creeTableau(String [] sp){
    EnsembleCaracteres tableau = new EnsembleCaracteres ();
    double posX=this.x;
    double posY=this.y;

    for(int i=sp.length-1;i>=0;i--){
      for (int x=0; x<sp[i].length(); x++){
        tableau.ajouteCar(posX,posY,sp[i].charAt(x));
        posX+=1;
      }
      posX=this.x;
      posY+=1;
    }
    return tableau;
  }

  public EnsembleCaracteres getEnsembleCaracteres(){
    return sprite;
  }

  //Renvoi le maxX de l'obstacle, pour savoir sa largeur
  public double getMaxX(){
    double max = sprite.getFirst().getX();
    for(CaracterePositionne m:sprite.getCarac()){
      if(m.getX()>max){
        max=m.getX();
      }
    }
    return max;
  }

  //renvoi la hauteur de l'obstacle
  public double getMaxY(){
    double max = sprite.getFirst().getY();
    for(CaracterePositionne m:sprite.getCarac()){
      if(m.getY()>max){
        max=m.getY();
      }
    }
    return max;
  }

}
