import java.lang.Math;

public class Projectile{
  double x,y,vitesseX,vitesseY,aY,vent;
  EnsembleCaracteres z;

  public Projectile(double x,double y){
    this.x=x;
    this.y=y;
    this.z=new EnsembleCaracteres();
    this.aY=-0.6;
  }

  //Permet de set les vitesses en X et Y.
  public void setVitesses(double x , double y){
    this.vitesseX=x;
    this.vitesseY=y;
  }
  public EnsembleCaracteres getEnsembleCaracteres(){
    return z;
  }

  public double getX(){
    return this.x;
  }

  public double getY(){
    return this.y;
  }

  //retourne le X max de z
  public double getMaxX(){
    double max = z.getFirst().getX();
    for(CaracterePositionne m:z.getCarac()){
      if(m.getX()>max){
        max=m.getX();
      }
    }
    return max;
  }

  //retourne le Y max de z
  public double getMaxY(){
    double max = z.getFirst().getY();
    for(CaracterePositionne m:z.getCarac()){
      if(m.getY()>max){
        max=m.getY();
      }
    }
    return max;
  }

  public void ajouter(double x, double y, char c){
    z.ajouteCar(x,y,c);
  }

  public boolean estDans(Fenetre f){
    if(f.getNbColonnes() >= getMaxX() && f.getNbLignes() >= getMaxY() && getY() > 0 && getX() > 0){
      return false;
    }
    else{
      return true;
    }
  }

  //Fait bouger le projectile selon les formules données, le vent est prit en compte.
  public void bouge(Vent vent){
    double deltaT =1/(Math.sqrt( vitesseX * vitesseX + vitesseY * vitesseY ));
    vitesseY += this.aY;
    //Prise en compte de la direction du vent
    if(vent.getDirection()==0){vitesseX -= vent.getForce();}
    else{vitesseX += vent.getForce();}

    this.x+=this.vitesseX*deltaT;
    this.y+=this.vitesseY*deltaT;
    for(CaracterePositionne c : z.getCarac()){
      c.x+= vitesseX * deltaT ;
      c.y+= vitesseY * deltaT ;
    }
  }

  //verifie si le projectile touche ou non la cible
  public boolean touche(Cible d){
    double maxCibleX=d.getMaxX();
    double maxCibleY=d.getMaxY()+1;//+1 pour etre fidele au dessin affiché
    double maxProjectileX=getMaxX();
    double maxProjectileY=getMaxY()+1;//+1 pour etre fidele au dessin affiché
    if(maxProjectileX >= d.x && maxProjectileX <= maxCibleX || getX() >= d.x && getX() <= maxCibleX){
      if(maxProjectileY >= d.y && maxProjectileY <= maxCibleY){
        return true;
      }
      else{
        if(getY() >= d.y && getY() <= maxCibleY){
          return true;
        }
      }
    }
    return false;
  }

  //indique si l'obstacle a ete touché
  public boolean toucheO(Obstacle o){
    double maProjectileX=getMaxX();
    double maProjectileY=getMaxY();
    double maObstacleX=o.getMaxX();
    double maObstacleY=o.getMaxY();
    if(maObstacleY > maProjectileY && o.x < maProjectileX && maObstacleX>maProjectileX){
      System.out.println("Tu as touché l'obstacle !!");
      return true;
    }
    return false;
  }

  //creer le projectile par defaut (la bombe)
  public void creerProjectileDefaut(){
    ajouter(getX(),getY(),'\\');
    ajouter(getX()+1,getY(),'#');
    ajouter(getX()+2,getY(),'/');
    ajouter(getX(),getY()+1,'/');
    ajouter(getX()+2,getY()+1,'\\');
  }
}
