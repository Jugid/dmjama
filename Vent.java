import java.util.Random;

public class Vent{
  int direction;
  double force;
  public Vent(){
    Random r = new Random();
    //generation d'une force aléatoire. Pour que cette derniere ne soit pas trop forte, je divise le nombre trouvé par 3
    force=r.nextDouble()/3;
    //Direction aléatoire entre 0 et 1, permet de savoir si la direction est gauche ou droite
    direction=r.nextInt(2);
  }

  int getDirection(){
    return direction;
  }

  double getForce(){
    return force;
  }

  //renvoi la phrase pour l'inscrire dans la console.
  String getPhrase(){
    if(this.direction==0){
      return "vient de droite";
    }
    else{
      return "vient de gauche";
    }
  }

}
